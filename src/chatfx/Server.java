/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatfx;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 *
 * @author Stefano
 */
    public class Server extends NetworkConnection {

    private int port;
    private String nomeServer;

    public Server(int port, String nomeServer, Consumer<Serializable> messaggioInArrivo) {
        super(messaggioInArrivo);
        this.port = port;
        this.nomeServer = nomeServer;
    }
    @Override
    protected boolean isServer() {
        return true;
    }

    @Override
    protected String getIp() {
        return "127.0.0.1";
    }

    @Override
    protected int getPort() {
        return port;
    } 

    public String getNomeServer() {
        return nomeServer;
    }
}
